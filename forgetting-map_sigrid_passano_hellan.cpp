// Sigrid Passano Hellan, 
// November 2016
//
// Forgetting map coding test for Origami Energy


#include <iostream>

using namespace std;

typedef int key_type; // Datatype for the keys used in the forgetting map.
typedef int content_type; // Datatype for the content stored in the forgetting map.

typedef unsigned int use_type; // Used to store how many times the content has been accessed.
#define NOT_FOUND -1; // Value returned by find() if the key was not found.

class cont{ // The content in each entry of the map.
    public:
        key_type key;
        content_type content;
        use_type use;
        void print();
} ;

struct linked_list{ // Used to link map entries together.
    cont node;
    linked_list* tail;
} ;

class Forgetting_map{ 
    unsigned int max_size;
    linked_list* head = NULL;
    public:
        Forgetting_map (int); // Constructor
        content_type find(key_type); // Return content associated with key.
        void print_map(); // Print the content of the map to terminal.
        void add(key_type, content_type, use_type); // Add a key-content association.
        void sort(); // Sort the map by use of entries.
};


void Forgetting_map::add(key_type key, content_type content, use_type use = 0){
    unsigned int count = 0; // Used to count number of entries.
    cont new_node; // New node with key - content association.
    new_node.key = key;
    new_node.content = content;
    new_node.use = use;
    linked_list* prev = NULL; // Used to keep track of previous entry.
    linked_list* two_prev = NULL; // Used to keep track of entry before previous.
    linked_list* copy = head; // Used to iterate through map.

    while((copy != NULL) && (copy->node.use > use) && (count < max_size-1)){ // Find correct location for insertion.
        two_prev = prev;
        prev = copy;
        copy = copy->tail;
        count++;
    }
    if(count == 0){ // If entry is to be inserted at the start of the list.
        linked_list* tmp_list = new linked_list;
        tmp_list->node = new_node;
        tmp_list->tail = head;
        head = tmp_list;
    }
    else{
        linked_list* new_list = new linked_list;
        new_list->node = new_node;
        new_list->tail = copy;
        prev->tail = new_list;
    }
    // Remove superfluous entries. 
    copy = head;
    prev = NULL;
    count = 0;
    while((copy != NULL) && (count < max_size)){ // Find entry / entries to remove.
        prev = copy;
        copy = copy->tail;
        count++;
    }
    if(count == max_size){ // Clean up.
        prev->tail = NULL;
        while(copy != NULL){
            linked_list* tmp = copy->tail;
            delete copy;
            copy = tmp;
        }
    }
}

int main(){

    // TESTS.
    //
    unsigned int size = 2;
    Forgetting_map my_map (size); // Start a map with max size 2.

    key_type key_1 = 1;
    key_type key_2 = 2;
    key_type key_5 = 5;

    // Add three entries.
    my_map.add(1, 28);
    my_map.add(2,  7);
    my_map.add(5, 30);
    cout << "\nmap (size " << size << ") after adding three entries:" << endl;
    my_map.print_map();

    // Look up entries.
    cout << "\nlook up entries:" << endl;
    content_type cont_1 = my_map.find(key_1);
    cout << "Key: " << key_1 << "\tContent: " << cont_1 << endl;
    content_type cont_2 = my_map.find(key_2);
    content_type cont_5 = my_map.find(key_5);
    cont_5 = my_map.find(key_5);
    cont_5 = my_map.find(key_5);
    cout << "Key: " << key_5 << "\tContent: " << cont_5 << endl;
    cout << "map after looking up entries" << endl;
    my_map.print_map();

    // Re-add first entry, give priority.
    my_map.add(key_1, 28, 6);
    cout << "\nmap after re-adding first entry" << endl;
    my_map.print_map();

    return 0;
}

Forgetting_map::Forgetting_map(int a){
    // Forgetting map constructor. 
    max_size = a;
}

void Forgetting_map::print_map(){
    // Prints to terminal all the currently stored associations. 
    cout << "Print map" << endl;
    linked_list* pointer = head;
    while(pointer != NULL){
        pointer->node.print();
        pointer = pointer->tail;
        cout << endl;
    }
}


content_type Forgetting_map::find(key_type key){
    // Returns either the wanted content associated with the key or NOT_FOUND,
    // a value used to show that the key is not used.
    // Sorts the map if the key was found, and increments that key's use counter. 
    linked_list* copy = head;
    while((copy != NULL) && (copy->node.key != key)){
        copy = copy->tail;
    }
    if(copy == NULL){
        return NOT_FOUND;
    }
    copy->node.use++;
    sort();
    return copy->node.content;
}

void Forgetting_map::sort(){
    // Used to sort the map. Called after find() is used to make sure the map stays ordered by
    // use. 
    bool done = true;
    linked_list* two_prev = NULL;
    linked_list* prev = head;
    linked_list* copy = head->tail;
    while(copy != NULL){
        if(copy->node.use > prev->node.use){
            done = false;
            cont tmp_node = prev->node;
            prev->node = copy->node;
            copy->node = tmp_node;
        }
        else{
            two_prev = prev;
            prev = copy;
            copy = copy->tail;
        }
    }
    if (done == false){
        sort();
    }
}

void cont::print(){
    // Used to print an entry in the map
    cout << "Key: " << key << endl;
    cout << "Content: " << content << endl;
    cout << "Use: " << use << endl;
}
